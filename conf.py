# Config fields stored as python file to avoid verbosity in code

# Strips header and footer from text, since these include direct mentions of the bill sponsors
STRIP = True

# For individual lookup against the Riksdag API
URL = "http://data.riksdagen.se/dokumentlista/?sok=&doktyp=mot&rm=&from=&tom=&ts=&bet=&tempbet=&nr=&org=&iid={}&webbtv=&talare=&exakt=&planering=&sort=rel&sortorder=desc&rapport=&utformat=json&a=s#soktraff"

# Paths
corpus_root = 'data/'
dump_path = corpus_root + 'json/'
corpus_train_path = corpus_root + 'corpus.json'
corpus_test_path = corpus_root + 'corpus-test.json'
corpus_dev_path = corpus_root + 'corpus-dev.json'

# Named fields
FIELD_ROOT = 'dokumentstatus'
FIELD_DOC = 'dokument'
FIELD_DOC_ID = 'dok_id'
FIELD_HTML = 'html'
FIELD_PERSON = 'dokintressent'
FIELD_PERSON_LIST = 'intressent'
FIELD_PERSON_PARTY = 'partibet'
TARGET_FIELD = 'party'

# Manual weights for naive bayes
prior_weights = {
	's' : 0.1,
	'mp': 0.15,
	'm' : 0.15,
	'kd': 0.15,
	'v' : 0.20,
	'c' : 0.20,
	'fp': 0.15,
	'nyd': 0.15
}