import glob
import json
import random
import simplejson
import argparse

import conf
import tools


def make_json(train_corpus_path, test_corpus_path, dev_corpus_path, training_size, test_size, dev_size):

	corpus_meta_paths = random.shuffle(glob.glob(conf.dump_path + '*.json'))

	no_files = len(corpus_meta_paths)
	no_extracted = training_size + test_size + dev_size

	if no_extracted > no_files:
		print('Desired number of test({}) and training({}) docs exceeding docs in directory({})'
			  .format(test_size + dev_size, training_size, no_files))
		exit(1)

	documents_train = {}
	documents_test = {}
	documents_dev = {}

	for (path, index) in zip(corpus_meta_paths, range(no_extracted)):
		print ('Reading file {}\t of {}'.format(index, no_extracted))

		meta_file = open(path, 'r')
		corpus_metadata = simplejson.load(meta_file)
		meta_file.close()

		doc_id = tools.doc_id_from_json(corpus_metadata)
		text = tools.text_from_json(corpus_metadata)
		party = tools.party_from_json(corpus_metadata)

		# Get sponsor parties, discard for no party data
		if party is None:
			print ('Bad party data, discarding...')
			continue

		if index < training_size:
			documents_train[doc_id] = {'party': party, 'text': text}
		elif index < training_size + test_size:
			documents_test[doc_id] = {'party': party, 'text': text}
		else:
			documents_dev[doc_id] = {'party': party, 'text': text}

	print('Training corpus size: {}'.format(len(documents_train)))
	print('Test corpus size: {}'.format(len(documents_test)))
	print('Dev corpus size: {}'.format(len(documents_dev)))

	with open (train_corpus_path, 'w') as outfile:
		json.dump(documents_train, outfile)
	with open (test_corpus_path, 'w') as outfile:
		json.dump(documents_test, outfile)
	with open (dev_corpus_path, 'w') as outfile:
		json.dump(documents_dev, outfile)

parser = argparse.ArgumentParser()
parser.add_argument("-train", help="Training corpus size (approx)", default=1000)
parser.add_argument("-test", help="Testing corpus size (approx)", default=0)
parser.add_argument("-dev", help="Dev testing corpus size (approx)", default=100)
args = parser.parse_args()

make_json(conf.corpus_train_path, conf.corpus_test_path, conf.corpus_dev_path, int(args.train), int(args.test), int(args.dev))