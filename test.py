import tools
import sys

def run_test(classifier, feature_funct, corpus_path, filter_classes):
	print('== Testing classifier ==')

	test_set = tools.build_data_set(corpus_path, filter_classes)

	features = feature_funct(test_set['data'])
	preds = classifier.predict(features).tolist()
	targs = list(test_set['target'])
	classes = set(preds + targs)

	confusion_matrix = {}
	for pred in classes:
		confusion_matrix[pred] = {}
		for ref in classes:
			confusion_matrix[pred][ref] = 0

	for pred, ref in zip(preds, targs):
		confusion_matrix[pred][ref] += 1

	__print_confusion(confusion_matrix)

def __precision_recall(matrix):

	smoothing = 0.0001

	true_positive = {}
	for cat in matrix:
		true_positive[cat] = matrix[cat][cat]

	precision = {}
	for pred in matrix:
		precision[pred] = true_positive[pred] / (smoothing + sum([matrix[pred][ref] for ref in matrix]))

	recall = {}
	for ref in matrix:
		recall[ref] = true_positive[ref] / (smoothing + sum([matrix[pred][ref] for pred in matrix]))

	return precision, recall

def __print_confusion(matrix):
	print('== PLOTTING CONFUSION MATRIX ==')

	precision, recall = __precision_recall(matrix)

	sys.stdout.write('prd\\ref')
	for cat in matrix:
		sys.stdout.write('\t{}'.format(cat))

	print('\tPRECISION:')

	for pred in matrix:
		sys.stdout.write(pred)
		for ref in matrix[pred]:
			sys.stdout.write('\t{}'.format(matrix[pred][ref]))
		print('\t{}'.format(precision[pred]))
	sys.stdout.write('RECALL:')
	for cat in matrix:
		sys.stdout.write('\t{:.4f}'.format(recall[cat]))
	print('')
	average_recall = sum(recall.values())/len(recall)
	average_precision = sum(precision.values())/len(precision)
	f1 = 2 * average_precision * average_recall / (average_precision + average_recall)
	print('Average recall: {}'.format(average_recall))
	print('Average precision: {}'.format(average_precision))
	print('Accuracy: {}'.format(f1))