import argparse

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

import conf
import tools
import test

def build_feature_vector(data_set, tf_idf):
	print('== Building feature vectors ==')

	tf_subl = tf_idf[0] == 'l'
	idf_t = tf_idf[1] == 't'
	norm = None
	if tf_idf[2] == 'c':
		norm = 'l2'

	count_vect = CountVectorizer()
	X_train_counts = count_vect.fit_transform(data_set['data'])

	tfidf_transformer = TfidfTransformer(use_idf=idf_t, sublinear_tf=tf_subl, norm=norm)
	X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts, y=data_set['target'])
	
	return X_train_tfidf

def train_classifier(feature_vector, data_set, params):
	print('== Training classifier ==')
	classifier = None

	if params['classifier'] == 'naive-bayes':
		print('Training Naive Bayes.')
		print('Using uniform priors => {}'.format(params['uniform-prior']))
		print('Using manual priors => {}'.format(params['manual-prior']))
		if params['manual-prior']:
			print('Manual priors:')
			manual_priors = tools.get_manual_priors()
			for party in conf.prior_weights:
				print('{} => {}'.format(party, conf.prior_weights[party]))
			classifier = MultinomialNB(fit_prior=False, class_prior=manual_priors)
		else:
			classifier = MultinomialNB(fit_prior=not params['uniform-prior'])

	elif params['classifier'] == 'svm':
		print('Training Support Vector Machine with RBF')
		print('C => {}'.format(params['svm-c']))
		print('gamma => {}'.format(params['svm-gamma']))
		classifier = SVC(kernel='rbf', C=float(params['svm-c']), gamma=float(params['svm-gamma']))

	elif params['classifier'] == 'nearest-neighbour':
		print('Training Nearest-neighbour')
		classifier = KNeighborsClassifier()

	else:
		print('Classifier {} could not be found. Exiting...'.format(params['classifier']))
		exit(1)


	return classifier.fit(feature_vector, data_set['target'])

def get_params(args):
	params = {
		'classifier': args.cls,
		'manual-prior': args.nb_manual_prior,
		'uniform-prior': args.nb_uniform_prior,
		'svm-gamma': args.svm_gamma,
		'svm-c': args.svm_c
	}
	return params

parser = argparse.ArgumentParser()
parser.add_argument("-model", help="Model export path")
parser.add_argument("-dev", action='store_true', help="Boolean: Evaluate parameters on dev set", default=False)
parser.add_argument("-filter", help="Train only on selected parties, example: s,v,mp")
parser.add_argument("-cls", help="Classifier type: svm | naive-bayes | nearest-neighbour. Default naive-bayes", default='naive-bayes')
parser.add_argument("-nb-manual-prior", action='store_true', help='Boolean: Use custom priors from conf.py')
parser.add_argument("-nb-uniform-prior", action='store_true', help='Boolean: Use uniform priors')
parser.add_argument("-svm-gamma", help="Set gamma parameter for SVM")
parser.add_argument("-svm-c", help="Set C parameter for SVM")
parser.add_argument("-tfidf", help="Set Tf/Idf scheme: (l|n)(t|n)(c|n). Default ltc", default='ltc')

args = parser.parse_args()
params = get_params(args)

data_set = tools.build_data_set(conf.corpus_train_path, args.filter)
feature_vector = build_feature_vector(data_set, args.tfidf)
classifier = train_classifier(feature_vector, data_set, params)

if args.dev:
	feature_funct = tools.feature_vector_closure(data_set)
	test.run_test(classifier, feature_funct, conf.corpus_dev_path, args.filter)

if args.model is not None:
	tools.write_model_to_file(args.model, classifier, data_set)
