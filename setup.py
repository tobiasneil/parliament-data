from setuptools import setup


setup(name='parliament-classifier',
      version='0.1',
      packages=[],
      install_requires=[
		  'scipy>=0.9'
          'nltk',
		  'simplejson',
		  'beautifulsoup4',
		  'sklearn',
		  'requests'
      ],
      zip_safe=False)

def download_tokenizer():
	import nltk
	nltk.download('punkt')

download_tokenizer()