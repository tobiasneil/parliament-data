import argparse
import glob

import test
import tools
import requests
import conf

def get_url(pers_id):
	return conf.URL.format(pers_id)

def get_docs_from_url(pers_id):
	docs = []
	resp = requests.get(get_url(pers_id))
	json_obj = resp.json()
	doc_urls = map(lambda x: 'http:' + x['dokument_url_text'], json_obj['dokumentlista']['dokument'])
	for doc_url in doc_urls:
		text = requests.get(doc_url).text
		docs.append(tools.preprocess(text))
	return docs

def get_docs(doc_path):
	paths = glob.glob(doc_path + '*')
	docs = []
	for path in paths:
		doc_file = open(path, 'r')
		doc = "".join(doc_file.readlines())
		docs.append(tools.preprocess(doc))
	return docs

def classify(classifier, feature_funct, docs):
	features = feature_funct(docs)
	predictions = classifier.predict(features).tolist()

	frequencies = {}
	for prediction in predictions:
		if prediction not in frequencies:
			frequencies[prediction] = 0
		frequencies[prediction] += 1

	for cls in frequencies:
		print('{} => {}'.format(cls, frequencies[cls]))

parser = argparse.ArgumentParser()
parser.add_argument("-model", help='Path to model')
parser.add_argument("-path", help='Path to directory of plain-text documents')
parser.add_argument("-test", action='store_true', default=False, help='Evaluates model against the test set')
parser.add_argument("-person", help='Person ID in the Parliament API. Classifies 20 latest bills')
args = parser.parse_args()

classifier, feature_funct = tools.read_model_from_file(args.model)

docs = None

if args.path is not None:
	docs = get_docs(args.path)
elif args.person is not None:
	docs = get_docs_from_url(args.person)
elif args.test:
	test.run_test(classifier, feature_funct, conf.corpus_test_path, None)

if not args.test:
	classify(classifier, feature_funct, docs)