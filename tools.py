import pickle
import re
import json
import nltk
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from conf import *

def build_data_set(corpus_path, filter_classes):
	print ('== Building data set ==')

	corpus_file = open(corpus_path,'r')
	corpus = json.load(corpus_file)
	corpus_file.close()

	parties = {}
	data_set = {'data': [], 'target': []}
	for doc in corpus.keys():
		if filter_classes is None or corpus[doc]['party'] in filter_classes:
			if corpus[doc]['party'] not in parties:
				parties[corpus[doc]['party']] = 0
			parties[corpus[doc]['party']] += 1
			data_set['data'].append(preprocess(corpus[doc]['text']))
			data_set['target'].append(corpus[doc]['party'])
	return data_set

def get_manual_priors():
	return list(map(lambda x: prior_weights[x], sorted(prior_weights.keys())))

def tokenize(text):
	tokens = nltk.word_tokenize(text)
	return "".join(tokens)

def doc_id_from_json(json):
	return json[FIELD_ROOT][FIELD_DOC][FIELD_DOC_ID]

def party_from_json(json):
	if FIELD_PERSON not in json[FIELD_ROOT] or json[FIELD_ROOT][FIELD_PERSON] is None:
		print('No party information. Aborting.')
		return None

	person_list = json[FIELD_ROOT][FIELD_PERSON][FIELD_PERSON_LIST]
	if not isinstance(person_list,list):
		person_list = [person_list]

	person_list = list(filter(lambda person: person[FIELD_PERSON_PARTY] is not None, person_list))

	if len(set(map(lambda person: person[FIELD_PERSON_PARTY].lower(), person_list))) > 1:
		print('Several party sponsors. Aborting.')
		return None

	party = person_list[0][FIELD_PERSON_PARTY].lower()
	if party == '-':
		return None
	if party == 'kds':
		party = 'kd'
	elif party == 'vpk':
		party = 'vp'
	elif party == 'l':
		party = 'fp'

	return party

def text_from_json(json):
	text = BeautifulSoup(json[FIELD_ROOT][FIELD_DOC][FIELD_HTML], 'html.parser').get_text()
	return text

def write_model_to_file(path, classifier, data_set):
	feature_vector_funct = feature_vector_closure(data_set)
	export_obj = {'classifier': classifier,
				  'feature_vector_funct': feature_vector_funct}

	pickle.dump(export_obj, open(path, 'wb'))

def read_model_from_file(path):
	import_obj = pickle.load(open(path, 'rb'))
	return import_obj['classifier'], import_obj['feature_vector_funct']

class feature_vector_closure:
	def __init__(self, data_set):
		self.count_vect = CountVectorizer()
		X_train_counts = self.count_vect.fit_transform(data_set['data'])
		self.tfidf_transformer = TfidfTransformer()
		self.tfidf_transformer.fit_transform(X_train_counts)

	def __call__(self, vector):
		X_new_counts = self.count_vect.transform(vector)
		X_new_tfidf = self.tfidf_transformer.transform(X_new_counts)
		return X_new_tfidf

def strip_text(text):
	if not STRIP:
		return text
	exp = re.compile('Stockholm den [0-9]+ [\w]+ [0-9]+')
	stripped = []
	for line in text.split('\n')[3:]:
		if exp.search(line) is not None:
			break
		stripped.append(line)
	return "".join(stripped)

def preprocess(text):
	if STRIP:
		text = strip_text(text)
	return tokenize(text)